$('.modal').click(function (e) {
    e.preventDefault(); 
    var linkModal = $(this).attr('href');

    $('body').addClass('open-modal');
    $(linkModal).fadeIn();
});

$('.close-modal, .bt-close-modal').click(function (e) {
    e.preventDefault(); 
    $('body').removeClass('open-modal');
    $(this).parents('.custom-modal').fadeOut();
    
});

$('.show-thank').click(function (e) {
 e.preventDefault(); 
 $(this).parents('.custom-modal').fadeOut();

$('#thank').fadeIn();
});

if ($(window).width() < 1200) {
    if($('.header-right .bg-plan').length){
        $('.header-right .bg-plan').remove();
        $('.header-right').prepend('<div class="bg-plan"></div>');
    }
    else {
        $('.header-right').prepend('<div class="bg-plan"></div>');
    }

    $('.bg-plan').click(function (e) {
    e.preventDefault(); 
    $(this).fadeOut();
    $('.show-hide').removeClass('open');
    $('.header-nav').fadeOut();
    $('body').removeClass('open-modal');
    });
}

$('.show-hide').click(function (e) {
 e.preventDefault(); 
  $(this).toggleClass('open');
  $('.header-nav').fadeToggle();
  $('.bg-plan').fadeIn();
  $('body').addClass('open-modal');
});

$('.feed-carus-init').slick({
    dots: true,
    infinite: true,
    speed: 300,
    slidesToShow: 1,
    centerMode: true,
    variableWidth: true,
    responsive: [
        {
          breakpoint: 767,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            variableWidth: false,
            centerMode: false,
            infinite: true,
            dots: true
          }
        },
        // {
        //   breakpoint: 600,
        //   settings: {
        //     slidesToShow: 2,
        //     slidesToScroll: 2
        //   }
        // },
        // {
        //   breakpoint: 480,
        //   settings: {
        //     slidesToShow: 1,
        //     slidesToScroll: 1
        //   }
        // }
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
      ]
});

if ($(window).width() < 767) {
    $('.bt-mob-cat').click(function (e) {
     e.preventDefault(); 
        $('.fast-menu ul').slideToggle();
    });

    $(document).click(function(e) {
        if (!$(e.target).is('.bt-mob-cat, .bt-mob-cat *, .fast-menu, .fast-menu *')) { 
            /* тут указываем блоки которые недолжны реагировать на закрытие. В нашем случае это блок самой кнопки .block-bt и все что внутри нее (.block-bt *) */
            $('.fast-menu ul').slideUp(); // Закрываем сам блок
        }
    });
}


if($('#numbers').length) {
    AOS.init({
        //disable: 'phone',
    });
}

document.addEventListener('aos:in:super-duper', ({ detail }) => {
console.log('animated in', detail);

    $( ".one-number-val" ).each(function( index ) {
        $(this).empty();

        var number = $(this).attr('data-num');
        var numArray = number.split("");

        for(var i=0; i<numArray.length; i++) { 
            numArray[i] = parseInt(numArray[i], 10);
            $(this).append('<span class="digit-con"><span class="digit'+i+'">0<br>1<br>2<br>3<br>4<br>5<br>6<br>7<br>8<br>9<br></span></span>');	
        }

        var increment = $(this).find('.digit-con').outerHeight();
        var speed = 1000;

        for(var i=0; i<numArray.length; i++) {
            $(this).find('.digit'+i).animate({top: -(increment * numArray[i])}, speed);
        }

        $(this).find(".digit-con:nth-last-child(3n+4)").after("<span class='comma'>,</a>");
      });

	


});


if ($(window).width() < 1200) {
    var scroll = $(window).scrollTop();

    if (scroll >= 10) {
        $("header").addClass("darkHeader");
    } else {
        $("header").removeClass("darkHeader");
    }

    $(window).scroll(function() {    
        var scroll = $(window).scrollTop();

        if (scroll >= 10) {
            $("header").addClass("darkHeader");
        } else {
            $("header").removeClass("darkHeader");
        }
    });
}

if($('table').length) {
    $( "table" ).wrap( "<div class='table-wrap'></div>" );
}